Defining the JavaScript Constants

Throughout this tutorial, we will be referring to certain constants to avoid hard coding values. These constants are shown below.
var defaults = {
  // CSS selectors and attributes that would be used by the JavaScript functions
  todoTask: "todo-task",
  todoHeader: "task-header",
  todoDate: "task-date",
  todoDescription: "task-description",
  taskId: "task-",
  formId: "todo-form",
  dataAttribute: "data",
  deleteDiv: "delete-div"
}, codes = {
  "1" : "#pending", // For pending tasks
  "2" : "#inProgress",
  "3" : "#completed"
};
Creating Tasks

Tasks are created using the following JavaScript function.
// Add Task
var generateElement = function(params) {
  var parent = $(codes[params.code]),
      wrapper;
 
  if (!parent) {
    return;
  }
 
  wrapper = $("<div />", {
    "class" : defaults.todoTask,
    "id" : defaults.taskId + params.id,
    "data" : params.id
  }).appendTo(parent);
 
  $("<div />", {
    "class" : defaults.todoHeader,
    "text": params.title
  }).appendTo(wrapper);
 
  $("<div />", {
    "class" : defaults.todoDate,
    "text": params.date
  }).appendTo(wrapper);
 
  $("<div />", {
    "class" : defaults.todoDescription,
    "text": params.description
  }).appendTo(wrapper);
};
The following code sample shows how a single task is generated.
generateElement({
  id: "123",
  code: "1",
  title: "My Uber Important Task",
  date: "5/2/2014",
  description: "I have to do a lot of steps to implement this task!"
});
Deleting Tasks

Removing tasks is fairly simple, and can be accomplished using the following function.
var removeElement = function(params) {
  $("#" + defaults.taskId + params.id).remove();
};
Saving Tasks in Local Storage

The tasks we create could be stored using a database, cookies, or a number of other technologies. However, in this application we�re going to use HTML5's local storage for its simplicity. In JavaScript, the variable localStorage stores all of this data. The following code sample shows how the to-do list data is retrieved from local storage.

1
var data = JSON.parse(localStorage.getItem("todoData"));
Each task would be stored within the data variable. An example task object is shown below.
{
  id : id, // Unique ID; timestamp is used here
  code: "1", // Code identifying the category
  title: title, // Title of the task
  date: date, // Due date
  description: description // Description of the task
}
We update the saved data in local storage using the following code.

1
localStorage.setItem("todoData", JSON.stringify(data));
Submitting the To-Do Form

When the to-do form is submitted, a new task is created and added to local storage, and the contents of the page are updated. The following function implements this functionality.
var addItem = function() {
  var inputs = $("#" + defaults.formId + " :input"),
      errorMessage = "Title can not be empty",
      id, title, description, date, tempData;
 
  if (inputs.length !== 4) {
    return;
  }
 
  title = inputs[0].value;
  description = inputs[1].value;
  date = inputs[2].value;
 
  if (!title) {
    generateDialog(errorMessage);
    return;
  }
 
  id = new Date().getTime();
 
  tempData = {
    id : id,
    code: "1",
    title: title,
    date: date,
    description: description
  };
 
  // Saving element in local storage
  data[id] = tempData;
  localStorage.setItem("todoData", JSON.stringify(data));
 
  // Generate Todo Element
  generateElement(tempData);
 
  // Reset Form
  inputs[0].value = "";
  inputs[1].value = "";
  inputs[2].value = "";
};
Implementing Drag and Drop

jQuery UI provides drag and drop functionality. We need to make each task draggable and each of the three categories droppable. To delete a task, we need to hide the delete area by default, and show it during the time an item is being dragged. Therefore, we first modify the generateElement() function slightly to first make the to-do list items draggable, and then make the delete area visible when the item is being drug.
$("." + defaults.todoTask).draggable();
 
// Add Task
var generateElement = function(params) {
  wrapper.draggable({
    start: function() {
      $("#" + defaults.deleteDiv).show();
    },
    stop: function() {
      $("#" + defaults.deleteDiv).hide();
    }
  });
...
};
Secondly, we need to add the droppable() function to each of the categories as the elements are supposed to be dropped in any one of the three areas.
// Adding drop function to each category of task
$.each(codes, function(index, value) {
  $(value).droppable({
    drop: function(event, ui) {
      var element = ui.helper,
          css_id = element.attr("id"),
          id = css_id.replace(options.taskId, ""),
          object = data[id];
 
      // Removing old element
      removeElement(object);
 
      // Changing object code
      object.code = index;
 
      // Generating new element
      generateElement(object);
 
      // Updating Local Storage
      data[id] = object;
      localStorage.setItem("todoData", JSON.stringify(data));
 
      // Hiding Delete Area
      $("#" + defaults.deleteDiv).hide();
    }
  });
});
Thirdly, we need to add some code to delete tasks when they are dropped in the delete area.
// Adding drop function to delete div
$("#" + options.deleteDiv).droppable({
  drop: function(event, ui) {
    var element = ui.helper,
        css_id = element.attr("id"),
        id = css_id.replace(options.taskId, ""),
        object = data[id];
 
    // Removing old element
    removeElement(object);
 
    // Updating local storage
    delete data[id];
    localStorage.setItem("todoData", JSON.stringify(data));
 
    // Hiding Delete Area
    $("#" + defaults.deleteDiv).hide();
  }
});